# Terms of Services and Privacy Policy

This policy governs your use of LEAF-Writer; by using LEAF-Writer, you accept this policy in full. If you disagree with this policy or any part of this policy, you must not use this website.

LEAF-Writer uses cookies. By using LEAF-Writer and agreeing to this policy, you consent to our use of cookies in accordance with the cookies policy [link].

The materials on LEAF-Writer are provided on an “as is” basis. LEAF-Writer makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties including, without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights.

Further, LEAF-Writer does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its website or otherwise relating to such materials or on any sites linked to this site.

## External links

LEAF-Writer does not regularly review all of the sites that link to it or are linked to from it and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by LEAF-Writer of the site. Use of any such linked website is at the user's own risk. You are advised to review the Privacy Policy of every site you visit, as this Privacy Policy does not apply to this third party content. LEAF-Writer has no control over and assumes no responsibility for the content, privacy policies or practices of any third party sites or services.  

## Legal requirements

The LINCS Project is based in Canada. The terms of this policy have been prepared in accordance with Canadian legal requirements.

## Data collection and storage

### What data is collected

### How data is collected

### How data is used

### How data is stored

### How long data is stored for

Personal data that we process for any purpose(s) shall not be kept for longer than is [****] necessary for the purpose(s) for which it is collected.

## Cookies

Most web browsers are set to accept cookies by default. If you prefer, you can choose to set your browser to remove cookies and/or to reject cookies. If you choose to remove cookies or reject cookies, this could affect certain features or services.

### What cookies we set

#### Necessary

#### Preferences

#### Statistics

#### Marketing

None.

#### Unclassified (miscellaneous)

None.

### How we use cookies

## Additional Terms

## Changes to this policy

This policy may be updated at any time. Updates will be posted on this page. You are encouraged to review this privacy notice frequently to be informed of how we are protecting your information.

## Contact us

If you have questions or comments about this policy, contact us at [cwrc-leaf@ualberta.ca](mailto:cwrc-leaf@ualberta.ca).

## Acknowledgements

In preparing this policy, LEAF-Writer built upon similar policy documents from KgBase, Metaphacts, and Mobi.
