import type { DialogBarProps } from '../../dialogs';

type State = {
  dialogBar: DialogBarProps[];
};

export const state: State = {
  dialogBar: [],
};
