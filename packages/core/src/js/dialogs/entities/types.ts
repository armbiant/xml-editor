export interface SchemaDialog {
    show: (config: any) => void;
    destroy: () => void;
}