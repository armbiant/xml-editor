module.exports = {
  format: '{type}{scope}: {emoji}{subject}',
  maxMessageLength: 110,
  questions: ['scope', 'type', 'subject', 'body', 'breaking', 'issues' ],
  scopes: ['root', 'commons', 'core', 'storage service', 'validator'],
};
