FROM node:18.16-alpine

# Needed because some dependencies are fetch from git and alpine does not come with git
RUN apk add --no-cache git

RUN npm install ts-node -g

WORKDIR /app

COPY ./packages/commons/package.json .

RUN npm install

COPY ./packages/commons .

RUN NODE_OPTIONS=--max_old_space_size=4096 npm run build

## Note: pm2 should not be used with docker: https://github.com/goldbergyoni/nodebestpractices/blob/master/sections/docker/restart-and-replicate-processes.md
CMD ["ts-node", "./server/index.ts"]

EXPOSE 3000
